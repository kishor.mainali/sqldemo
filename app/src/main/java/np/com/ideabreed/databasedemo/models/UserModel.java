package np.com.ideabreed.databasedemo.models;

public class UserModel {

    private int id;
    private String name;
    private String address;
    private String email;
    private String contact;
    private String image_link;

    public UserModel(){
    }

    public UserModel(int id, String name, String address, String email, String contact,
                     String image_link){
        this.id = id;
        this.name = name;
        this.address = address;
        this.email = email;
        this.contact = contact;
        this.image_link = image_link;
    }

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getAddress(){
        return address;
    }

    public void setAddress(String address){
        this.address = address;
    }

    public String getEmail(){
        return email;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public String getContact(){
        return contact;
    }

    public void setContact(String contact){
        this.contact = contact;
    }

    public String getImage_link(){
        return image_link;
    }

    public void setImage_link(String image_link){
        this.image_link = image_link;
    }
}
