package np.com.ideabreed.databasedemo;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import np.com.ideabreed.databasedemo.database.DatabaseHandler;
import np.com.ideabreed.databasedemo.database.DbDatabase;
import np.com.ideabreed.databasedemo.models.DBUserModel;
import np.com.ideabreed.databasedemo.models.UserModel;

public class AddActivity extends AppCompatActivity {

    EditText nameInput, addressInput, emailInput, contactInput, imageInput;
    DatabaseHandler db;
    Button button;
    UserModel model;
    DbDatabase db1;
    DBUserModel userModel;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        db = new DatabaseHandler(this);

        db1 = DbDatabase.getInstance(this);

        nameInput = findViewById(R.id.name);
        addressInput = findViewById(R.id.address);
        emailInput = findViewById(R.id.email);
        contactInput = findViewById(R.id.contact_no);
        imageInput = findViewById(R.id.image_link);
        button = findViewById(R.id.addbutton);
        model = new UserModel();
        userModel = new DBUserModel();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){

                String name = nameInput.getText().toString();
                String address = addressInput.getText().toString();
                String email = emailInput.getText().toString();
                String contact = contactInput.getText().toString();
                String image = imageInput.getText().toString();

                //SQLITE db using SQLiteOpenHelper
                //                model.setName(name);
                //                model.setAddress(address);
                //                model.setEmail(email);
                //                model.setContact(contact);
                //                model.setImage_link(image);
                //
                //                db.insertPostIntoDb(model);

                //using Room Database
                userModel.setName(name);
                userModel.setAddress(address);
                userModel.setEmail(email);
                userModel.setContact(contact);
                userModel.setImage_link(image);

                db1.dao().inserUserIntoDb(userModel);


                Toast.makeText(AddActivity.this, "successfully inserted", Toast.LENGTH_SHORT)
                     .show();


            }
        });


    }
}
