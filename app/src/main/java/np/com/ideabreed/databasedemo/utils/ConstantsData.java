package np.com.ideabreed.databasedemo.utils;

public class ConstantsData {


    public static final String DATABASE_NAME = "usersDatabase";
    public static final int DATABASE_VERSION = 1;
    public static final String USERS_TABLE = "users";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String CONTACT = "contact";
    public static final String IMAGE_URL = "image_link";
    public static final String ADDRESS = "address";


}
